package br.com.itau.estruturaDados;

public class FilaBancaria {
	
	private int proximo = 1 ; 

	private MinhaFila filaNaoPreferencial = new MinhaFila();
	
	private MinhaFila filaPreferencial = new MinhaFila();
	
	
	public void enqueue(Object objeto, boolean ehPreferencial) {
	
		if(ehPreferencial) {
			
			filaPreferencial.enqueue(objeto);
		
		} 	else {
			
			
			filaNaoPreferencial.enqueue(objeto);
		}
	}
	
	public Object dequeue() throws FilaVaziaExeption {
		 
		Object objeto = null;
		if(isEmpty()) {
			
			throw new FilaVaziaExeption();
		}
		
		while(filaNaoPreferencial.isEmpty()) 
		{
			objeto = filaPreferencial.dequeue();
			proximo++;
		}
		while(filaPreferencial.isEmpty()) 
		{
			objeto = filaNaoPreferencial.dequeue();
			proximo++;
		}
		
		
		while(objeto == null) {
			
		if(proximo % 3 == 0) {

			
			objeto = filaPreferencial.dequeue();
			
		}
		else {
			
			objeto = filaNaoPreferencial.dequeue();
			}
		
		
		proximo++;
		}
		return objeto;
	}
	
	public boolean isEmpty() {
		return filaNaoPreferencial.isEmpty() && filaPreferencial.isEmpty();
	}
	
	public int tamanho() {
		return filaNaoPreferencial.tamanho() + filaPreferencial.tamanho();
	}	
	

}
