package br.com.itau.estruturaDados;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MinhaFilaTest {

	MinhaFila fila = new MinhaFila();
	
	@Test
	void test() {

		fila.enqueue("Helder");
		fila.enqueue("Juca");
		fila.enqueue("Joao");
		fila.enqueue("Jose");
		
		
		System.out.println(fila.dequeue());
		System.out.println(fila.dequeue());
		System.out.println(fila.dequeue());
		System.out.println(fila.dequeue());

	}

}
