package br.com.itau.estruturaDados;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class FilaBancariaTest {

	FilaBancaria fila = new FilaBancaria();

	@Test
	public void filaNormalTest() throws FilaVaziaExeption {

		fila.enqueue("Pedro", false);
		fila.enqueue("Paulo", false);
		fila.enqueue("Tiago", true);
		fila.enqueue("Juca", true);
		fila.enqueue("Matheus", false);
		fila.enqueue("Carlos", false);
		fila.enqueue("Maria", true);
		
		Assert.assertEquals("Pedro", fila.dequeue());
		Assert.assertEquals("Paulo", fila.dequeue());
		Assert.assertEquals("Tiago", fila.dequeue());
		Assert.assertEquals("Matheus", fila.dequeue());
		Assert.assertEquals("Carlos", fila.dequeue());
		Assert.assertEquals("Juca", fila.dequeue());
		Assert.assertEquals("Maria", fila.dequeue());
	}

	@Test
	public void filaUnicaTest() throws FilaVaziaExeption {
		fila.enqueue("Pedro", true);
		fila.enqueue("Paulo", true);
		fila.enqueue("Tiago", true);
		fila.enqueue("Maria", true);
		fila.enqueue("Jonas", true);


		Assert.assertEquals("Pedro", fila.dequeue());
		Assert.assertEquals("Paulo", fila.dequeue());
		Assert.assertEquals("Tiago", fila.dequeue());
		Assert.assertEquals("Maria", fila.dequeue());
		Assert.assertEquals("Jonas", fila.dequeue());
	}

}
