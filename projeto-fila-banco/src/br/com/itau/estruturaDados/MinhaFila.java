package br.com.itau.estruturaDados;

public class MinhaFila {

	private static final int ALOCACAO = 3;
	
	private Object[] arrayInterno = new Object[ALOCACAO];

	private int inseridos;
	
	public void enqueue(Object objeto) {
		
		if(inseridos == arrayInterno.length) 
		{
			Object[] novoArray = new Object[arrayInterno.length + ALOCACAO];
			
			for (int i = 0; i < arrayInterno.length; i++) {
			
				novoArray[i] = arrayInterno[i];
				
			}
			
			arrayInterno = novoArray;
		}
	
		arrayInterno[inseridos] = objeto;
		
		inseridos++;
	}
	
	public Object dequeue() {
		
		Object objeto = arrayInterno[0];
		
		for (int i = 1; i < inseridos; i++) {
			
			arrayInterno[i - 1] = arrayInterno[i]; 
		}
			arrayInterno[inseridos - 1] = null;
		
		return objeto;
	}
	
	public boolean isEmpty() {
		return inseridos == 0;
	}
	
	public int tamanho() {
		return inseridos;
	}
}